import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css'; // Asegúrate de tener un archivo App.css para tus estilos.

const App = () => {
  const [tasks, setTasks] = useState([]);
  const [users, setUsers] = useState([]);
  const [newTask, setNewTask] = useState({ title: '', description: '', assigned_to: '', status: 'Pendiente' });
  const [isAddTaskFormVisible, setIsAddTaskFormVisible] = useState(false);

  const url = 'http://localhost:8000/api';

  useEffect(() => {
    // Obtener todas las tareas al cargar el componente
    axios.get(url+ '/tasks')
      .then(response => setTasks(response.data.tasks))
      .catch(error => console.error('Error al obtener tareas:', error));

    // Obtener todos los usuarios al cargar el componente
    axios.get(url+ '/users')
      .then(response => setUsers(response.data.users))
      .catch(error => console.error('Error al obtener usuarios:', error));
  }, []);

  const handleAddTask = () => {
    axios.post(url+ '/tasks', newTask)
      .then(response => {
        setTasks([...tasks, response.data.task]);
        setNewTask({ title: '', description: '', assigned_to: '', status: 'Pendiente' });
        setIsAddTaskFormVisible(false);
      })
      .catch(error => console.error('Error al agregar tarea:', error));
  };

  const handleUpdateTaskStatus = (taskId, newStatus) => {
    axios.put(url+ `/tasks/${taskId}`, { status: newStatus })
      .then(response => {
        const updatedTasks = tasks.map(task =>
          task.id === taskId ? { ...task, status: response.data.task.status } : task
        );
        setTasks(updatedTasks);
      })
      .catch(error => console.error('Error al actualizar estado de tarea:', error));
  };

  const handleDeleteTask = (taskId) => {
    axios.delete(url+ `/tasks/${taskId}`)
      .then(() => {
        const updatedTasks = tasks.filter(task => task.id !== taskId);
        setTasks(updatedTasks);
      })
      .catch(error => console.error('Error al eliminar tarea:', error));
  };

  return (
    <div className="container">
      <h1>Kanban App</h1>

      <button className="add-task-button" onClick={() => setIsAddTaskFormVisible(true)}>Agregar Tarea</button>

      {isAddTaskFormVisible && (
        <div className="add-task-popup">
          <label>
            Nombre de la Tarea:
            <input
              type="text"
              value={newTask.title}
              onChange={e => setNewTask({ ...newTask, title: e.target.value })}
            />
          </label>
          <label>
            Descripción de la Tarea:
            <textarea
              value={newTask.description}
              onChange={e => setNewTask({ ...newTask, description: e.target.value })}
            />
          </label>
          <label>
            Asignada a:
            <select
              value={newTask.assigned_to}
              onChange={e => setNewTask({ ...newTask, assigned_to: e.target.value })}
            >
              <option value="">Seleccionar Usuario...</option>
              {users.map(user => (
                <option key={user.id} value={user.id}>{user.name}</option>
              ))}
            </select>
          </label>
          <button onClick={handleAddTask}>Agregar Tarea</button>
          <button onClick={() => setIsAddTaskFormVisible(false)}>Cancelar</button>
        </div>
      )}

      <div className="kanban-board">
        <div className="column">
          <h2>Pendiente</h2>
          {tasks.filter(task => task.status === 'Pendiente').map(task => (
            <div key={task.id} className="task">
              <div className="task-title">{task.title}</div>
              <div className="task-description">{task.description}</div>
              <div className="task-assignedTo">Asignada a: {task.assigned_user.name}</div>
              <button onClick={() => handleUpdateTaskStatus(task.id, 'En Proceso')}>Iniciar</button>
              <button onClick={() => handleDeleteTask(task.id)}>Eliminar</button>
            </div>
          ))}
        </div>

        <div className="column">
          <h2>En Proceso</h2>
          {tasks.filter(task => task.status === 'En Proceso').map(task => (
            <div key={task.id} className="task">
              <div className="task-title">{task.title}</div>
              <div className="task-description">{task.description}</div>
              <div className="task-assignedTo">Asignada a: {task.assigned_user.name}</div>
              <button onClick={() => handleUpdateTaskStatus(task.id, 'Pendiente')}>Volver a Pendiente</button>
              <button onClick={() => handleUpdateTaskStatus(task.id, 'Finalizado')}>Finalizar</button>
              <button onClick={() => handleDeleteTask(task.id)}>Eliminar</button>
            </div>
          ))}
        </div>

        <div className="column">
          <h2>Finalizado</h2>
          {tasks.filter(task => task.status === 'Finalizado').map(task => (
            <div key={task.id} className="task">
              <div className="task-title">{task.title}</div>
              <div className="task-description">{task.description}</div>
              <div className="task-assignedTo">Asignada a: {task.assigned_user.name}</div>
              <button onClick={() => handleUpdateTaskStatus(task.id, 'En Proceso')}>Reabrir</button>
              <button onClick={() => handleDeleteTask(task.id)}>Eliminar</button>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default App;
